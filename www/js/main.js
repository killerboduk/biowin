
var login = localStorage.getItem('login');


// var wordpress_url = 'http://localhost/wordpress_template/wp-content/plugins/alln1api/m_api.php';
// var wordpress_product = 'http://localhost/wordpress_template/wp-content/plugins/alln1api/?api_key=1e736ca30a127c054ff85169bd8a258e';
// var wordpress_pages = 'http://localhost/wordpress_template/wp-content/plugins/alln1api/?api_key=d54bd5a1d6c328bfc9ec888f4350033b';
// var wordpress_order = 'http://localhost/wordpress_template/wp-json/api/order';
// var current_user = 'http://localhost/wordpress_template/wp-content/plugins/alln1api/m_api.php?currentuser';
// var userData = 'http://localhost/wordpress_template/wp-content/plugins/alln1api/m_api.php?user';
// var order = 'http://localhost/wordpress_template/wp-content/plugins/alln1api/m_api.php?orders';
// var currency_link = 'http://localhost/wordpress_template/wp-content/plugins/alln1api/m_api.php?currencysymbol';
// var product_image = 'http://localhost/wordpress_template/wp-content/plugins/alln1api/m_api.php?productimage';
// var mobile_pages = 'http://localhost/wordpress_template/wp-content/plugins/alln1api/mobile/';
// var get_page = 'http://localhost/wordpress_template/wp-content/plugins/alln1api/m_api.php?page=';



var wordpress_url = 'https://biowinpharma.com/wp-content/plugins/alln1api/m_api.php';
var wordpress_product = 'https://biowinpharma.com/wp-content/plugins/alln1api/?api_key=365f0208a4819094945bafd51c86e8c0';
var wordpress_pages = 'https://biowinpharma.com/wp-content/plugins/alln1api/?api_key=23fac8a02025b5b2d8dc30e4dc7d6d01';
var wordpress_order = 'https://biowinpharma.com/wp-json/api/order';
var current_user = 'https://biowinpharma.com/wp-content/plugins/alln1api/m_api.php?currentuser';
var userData = 'https://biowinpharma.com/wp-content/plugins/alln1api/m_api.php?user';
var order = 'https://biowinpharma.com/wp-content/plugins/alln1api/m_api.php?orders';
var currency_link = 'https://biowinpharma.com/wp-content/plugins/alln1api/m_api.php?currencysymbol';
var product_image = 'https://biowinpharma.com/wp-content/plugins/alln1api/m_api.php?productimage';
var mobile_pages = 'https://biowinpharma.com/wp-content/plugins/alln1api/mobile/';
var get_page = 'https://biowinpharma.com/wp-content/plugins/alln1api/m_api.php?page=';



// https://biowinpharma.com/
// var wordpress_url = 'http://gregoriobalonzo.ml/wordpress/wp-content/plugins/alln1api/m_api.php';
// var wordpress_product = 'http://gregoriobalonzo.ml/wordpress/wp-content/plugins/alln1api/?api_key=1e736ca30a127c054ff85169bd8a258e';
// var wordpress_order = 'http://gregoriobalonzo.ml/wordpress/wp-json/api/order';
// var current_user = 'http://gregoriobalonzo.ml/wordpress/wp-content/plugins/alln1api/m_api.php?currentuser';
// var userData = 'http://gregoriobalonzo.ml/wordpress/wp-content/plugins/alln1api/m_api.php?user';
// var order = 'http://gregoriobalonzo.ml/wordpress/wp-content/plugins/alln1api/m_api.php?orders';
// var currency_link = 'http://gregoriobalonzo.ml/wordpress/wp-content/plugins/alln1api/m_api.php?currencysymbol';
// var product_image = 'http://gregoriobalonzo.ml/wordpress/wp-content/plugins/alln1api/m_api.php?productimage';
// var mobile_pages = 'http://gregoriobalonzo.ml/wordpress/wp-content/plugins/alln1api/mobile/';
//





$(".dropdown-trigger").dropdown();

product_page();
refresh();
getCurrency();

$("#menu a").click(function (e) {
    var page = $(this).attr('page');
    if(page == 'Product'){
        product_page();
        menu_hide();
    }
    if(page == 'Health'){
        $('#page_title').text('Health Records');
        $('#page_desc').text('Health records description text');
        $.get(''+mobile_pages+'health.php',function (data){
            $('#page_content_view').html(data);
        });
        menu_hide();
    }
    if(page == 'Account'){
        account();
        menu_hide();
    }
    if(page == 'Orders'){
        order_page();
        menu_hide();
    }
    if(page == 'Flyers'){
        $('#page_content_view').html('');
        $('#page_title').text('Weekly flyers');
        $('#page_desc').text('Our Weekly flyers description text');
        $.get(''+mobile_pages+'flyers.php',function (data){
            $('#page_content_view').html(data);
        });
        menu_hide();
    }
    if(page == 'Prescription'){
        $('#page_content_view').html('');
        $('#page_title').text('PRESCRIPTION REFILL');
        $('#page_desc').text('REFILL WITH YOUR PRESCRIPTION NUMBERS description text');
        $.get(''+mobile_pages+'prescription.php',function (data){
            $('#page_content_view').html(data);
        });
        menu_hide();
    }

});

function show_custom_page(id)
{
  get_custom_pages(id);
  menu_hide();
}

function get_wp_pages()
{
  $.get(wordpress_pages,function(data){
      $('#wp_pages').css('display','block');
      //$('#wp_list_pages').append("<li><a page='Product' href='#'>Product</a></li>");
      for(var a = 0; a <= data.length - 1; a++)
      {
        $('#wp_list_pages').append("<li><a page='custom_page' onclick='show_custom_page("+data[a].ID+");'  href='#'>"+data[a].post_title+"</a></li>");
        //console.log(data[a].post_title);
      }
  })
}

function get_custom_pages(id)
{
  $.get(get_page+''+id,function(data){
    $('#page_title').text('');
    $('#page_desc').text('');
    $('#page_content_view').html(data);
  })
}

function get_image()
{

}

function getCurrency()
{
    $.get(currency_link,function(data){
        localStorage.setItem('currency',data)
    })
}

function order_page()
{
    $('#page_title').text('MY ORDER');
    $('#page_desc').text('');
    $('#page_content_view').load('page/order.html');
}

function get_order()
{
    var user_id = localStorage.getItem('userid')
    $.get(order+'='+user_id,function(data){
        console.log(data);
    })
}

function refresh()
{
    var login_check = localStorage.getItem('userid');
    if(login_check == 0){
        hide_btn();
    }else{
        show_btn();
    }
}

function show_btn()
{
    $("#prod_btn").css('display','block');
    $("#acnt_edit_btn").css('display','block');
    $("#logout_btn").css('display','block');
    $("#orders_btn").css('display','block');
    $("#health_records").css('display','block');
}

function hide_btn()
{
    $("#prod_btn").css('display','none');
    $("#acnt_edit_btn").css('display','none');
    $("#logout_btn").css('display','none');
    $("#orders_btn").css('display','none');
    $("#health_records").css('display','none');
}

function currentuser()
{
    $.get(current_user,function(data){
        localStorage.setItem('userid',data);
    });
}

function getUserData()
{
    var userId = localStorage.getItem('userid');
    $.get(userData+'='+userId,function(data){
        var data = JSON.parse(data);
        console.log(data);
        localStorage.setItem('nickname',data.nickname);
        localStorage.setItem('firstname',data.first_name);
        localStorage.setItem('lastname',data.last_name);
        //billing
        localStorage.setItem('billing_address_1',data.billing_address_1);
        localStorage.setItem('billing_address_2',data.billing_address_2);
        localStorage.setItem('billing_city',data.billing_city);
        localStorage.setItem('billing_company',data.billing_company);
        localStorage.setItem('billing_country',data.billing_country);
        localStorage.setItem('billing_email',data.billing_email);
        localStorage.setItem('billing_first_name',data.billing_first_name);
        localStorage.setItem('billing_last_name',data.billing_last_name);
        localStorage.setItem('billing_phone',data.billing_phone);
        localStorage.setItem('billing_postcode',data.billing_postcode);
        localStorage.setItem('billing_state',data.billing_state);
        $('#user_fname').val(localStorage.getItem('firstname'));
        $('#user_lname').val(localStorage.getItem('lastname'));
        $('#user_email').val(localStorage.getItem('billing_email'));
        $('#billing_first_name').val(localStorage.getItem('billing_first_name'));
        $('#billing_last_name').val(localStorage.getItem('billing_last_name'));
        $('#billing_company').val(localStorage.getItem('billing_company'));
        $('#billing_address_1').val(localStorage.getItem('billing_address_1'));
        $('#billing_address_2').val(localStorage.getItem('billing_address_2'));
        $('#billing_city').val(localStorage.getItem('billing_city'));
        $('#billing_postcode').val(localStorage.getItem('billing_postcode'));
        $('#billing_country').val(localStorage.getItem('billing_country'));
        $('#billing_state').val(localStorage.getItem('billing_state'));
        $('#billing_phone').val(localStorage.getItem('billing_phone'));
        $('#billing_email').val(localStorage.getItem('billing_email'));
    })
}

function menu_hide()
{
    $("#menu").removeClass("active");
}

function show_error(){
    $('#error').html(`
      <div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">x</span><span class="sr-only">Close</span></button>
          <strong>Error!</strong> wrong username or password
      </div>
      `);
}
function login_btn_disabled(){
    $('#login_btn1').text('Please wait...');
    $('#login_btn1').attr('disabled',true);
}
function login_btn_back(){
    $('#login_btn1').text('Submit');
    $('#login_btn1').attr('disabled',false);
}



function login_status(){
    if(localStorage.getItem('login') == 0 ){
        return 0;
    }else{
        return 1;
    }
}



function product_page(){
    if(login_status(localStorage.getItem('login')) == 0 ){
        login_page();
    }else{
        $('#page_title').text('PRODUCT');
        $('#page_desc').text('product description text');
        $('#page_content_view').load('page/product.html');
        var varCurrency =  localStorage.getItem('currency');
        $.getJSON(wordpress_product, function(json) {
            var product_length = json.length;
            var product_data = json;
            //console.log("JSON Data: "+json[0].post_title+" "+ json[0].meta_value);
            for(var a = 0; a <= product_length - 1; a++){

                var content = `


<div class='col-md-4'>
<div class="panel panel-colorful">
    <div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title">`+json[a].post_title+`</h3>
    </div>
    <div class="panel-body">

        <p>`+json[a].post_content+`</p>
    </div>
    <div class="">
        <div class="btn btn-default" id="product_price">`+varCurrency+`  `+json[a].meta_value+`</div>
        <button type="button" onclick="order_product(`+json[a].ID+`)" class="btn btn-info pull-right" id="order_`+json[a].ID+`">ORDER</button>
    </div>
</div>
                    `;

                $('#loop_product').append(content);
            }

        });
    }
}

function login_user_now(data,username,password){
    if(data == '0'){
        show_btn();
        localStorage.setItem('login',1);
        localStorage.setItem('username',username);
        localStorage.setItem('password',password);
        currentuser();
        product_page();
    }else{
        show_error();
        login_btn_back();
    }
    login_btn_back();
}

function login_user_form(ee){
    login_btn_disabled();
    var username = $('#username').val();
    var password = $('#password').val();

    $.post(wordpress_url, $("#mobile_login_form").serialize(),function(data){
        login_user_now(data,username,password);
        get_wp_pages();
        login_btn_back();
    })
        .done(function() {
            //alert( "second success" );
        })
        .fail(function() {
            //alert( "error" );
        })
        .always(function() {
            //alert( "finished" );
        });

}

function login_page(){
    $('#page_content_view').load('page/login.html');
}
function account(){
    getUserData();
    $('#page_title').text('Account');
    $('#page_desc').text('account details');

    $('#page_content_view').load('page/account.html');
}

function logout(){
    hide_btn();
    var username = 0;
    var password = 0;
    localStorage.setItem('username',username);
    localStorage.setItem('password',password);
    window.location = 'index.html';
}

function order_product(id){
    var userid =  localStorage.getItem('userid');
    $('#order_'+id+'').attr('disabled',true);
    $('#order_'+id+'').text('please wait..');
    $.getJSON(wordpress_order+'?id='+id+'&&userid='+userid,function(data){
        if(data == 1){
            $('#order_'+id+'').text('Successfully Added');
            $('#order_'+id+'').attr('disabled',false);
        }
    });
}

function error(id){
    $('#'+id+'').html(`
        <div class="card-panel red lighten-2 white-text">wrong user or pass</div>
        `);
}
